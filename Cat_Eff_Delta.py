#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 11:18:16 2017

@author: weigand


Test the effective squeezing of cat states and three peak states in q and p
"""
import numpy as np
import Functions as F


def local_maxima(a):
    return np.r_[True, a[1:] > a[:-1]] & np.r_[a[:-1] > a[1:], True]


if __name__ == '__main__':

    delta = 0.2
    distance = np.sqrt(2 * np.pi)
    
    X = np.linspace(-60, 60, 2**13 + 1)
    
    c1 = F.cat(X, delta, distance)
    c1.fourier('q')
    c1.P()
    c1.plot(lim=(-10,10))
    print(c1.x[np.logical_and(local_maxima(c1.y) ,c1.y>0.1)])
    print(c1.y[np.logical_and(local_maxima(c1.y) ,c1.y>0.1)])
    

    print('Delta', delta)
    c = F.cat(X, delta, distance)
    
    print('Eff Delta of Cat (p)', c.mean_sigma(distance)[1])
    c.fourier('q')
    print('Eff Delta of Cat (q)', c.mean_sigma(distance)[1])
    print('Eff Delta of Cat (p) - Delta', c.mean_sigma(distance)[1] - 0.2)
    c = F.cat3(X, delta, distance)
    print('Eff Delta of 3 Peaks (p)', c.mean_sigma(distance)[1])
    c.fourier('q')
    print('Eff Delta of 3 Peaks (q)', c.mean_sigma(distance)[1])
    print('Eff Delta of 3 Peaks (q) - Delta', c.mean_sigma(distance)[1] - 0.2)
