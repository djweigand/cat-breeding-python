#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 11:18:16 2017

@author: weigand

Simulate breeding and extract feedback phases corresponding to the measurement record
"""


import numpy as np
from qubit_oscillator.misc.other import int_to_bin, bin_to_int

import Functions as F


def map_phases(record, spacing):
    M = len(record)
    bitstrings = int_to_bin(np.arange(2**M), M)
    
    phases = []
    for bitstring in bitstrings:
        phase = 0
        bitstring = list(bitstring)
        for i in np.arange(M,0,-1):
            bit = bitstring.pop()
            if i == 1:
                bitstring = [0]
            p = record[M-i][bin_to_int(bitstring)]
            phase += (-1)**bit * 2**((i-2)*0.5) * p
        phases.append(spacing * phase)
    return phases

    
def helper(arg):
    spacing = arg[3]
    np.random.seed()
    state, record = F.breed(*arg)
    state.fourier('p') 
    quality = state.mean_sigma(spacing)
    return quality, record


if __name__ == '__main__':
    delta = 0.3
    spacing = np.sqrt(2 * np.pi)
    M = 2
    repetitions = 10
    
    #X = np.linspace(-60, 60, 2**13 + 1)
    X = np.linspace(-25, 25, 2**11 + 1)
    
    arguments = [(X, delta, M, spacing, None, True) for i in range(repetitions)]
   
    qualities, records = list(zip(*[helper(arg) for arg in arguments]))
         
    for i in range(len(qualities)):
        quality = qualities[i]
        record_2 = records[i]
        record_1 = [[[r]] for r in record_2[0]]
        print("Mu: {}, Delta: {}".format(quality[0]/spacing, quality[1]))
        print("Record 2: "+ str(record_2))
        print("Phases 2: "+ str(map_phases(record_2, spacing)))
        print("Record 1: "+ str(record_1))
        print("Phases 1: "+ str([map_phases(r, spacing * np.sqrt(2)) for r in record_1]))




