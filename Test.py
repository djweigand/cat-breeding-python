#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 11:18:16 2017

@author: weigand
"""

import numpy as np
import matplotlib.pyplot as plt

import Functions as F

#from memory_profiler import profile



N_POINTS = 2**11 + 1         # use 2^k+1 values for romb integration


class Test():
    def __init__(self):
        pass

    def assert_almostequal(self, a, b, n=7):
        assert round(a - b, n) == 0, (a, b, n)

    def test_State(self):
        x = np.linspace(-50, 50, 2**12 + 1)
        for _ in range(10):
            y = (np.random.random(len(x)) - 0.5) * 5 * np.random.random()
            y[0] = 0
            y[-1] = 0
            state = F.State(x, y, type='wavef')
            self.assert_almostequal(state._norm(), 1)
            state = F.State(x, y, type='prob')
            self.assert_almostequal(state._norm(), 1)
        print('State test ok')

    def test_mean(self):
        p = np.linspace(-50, 50, 2**11 + 1)

        M1 = (np.random.sample(5) * 0.5 - 0.5) * 4
        for m1 in M1:
            M2 = (np.random.sample(5) * 0.5 - 0.5) * 4
            for m2 in M2:
                S = 0.1 + np.random.sample(5) * 1.9
                for s in S:
                    coh = F.coherent(p, s, (m2 + 1j * m1))
                    for d in (1, np.sqrt(2)):
                        m_s = coh.mean_sigma(d)
                        assert np.abs(m_s[0] - m1) < coh._delta_x
                        assert np.abs(m_s[1] - s) < coh._delta_x

        p = np.linspace(-1000, 1000, 2**15 + 1)
        M1 = (np.random.sample(5) * 0.5 - 0.5) * 4
        for m1 in M1:
            M2 = (np.random.sample(5) * 0.5 - 0.5) * 4
            for m2 in M2:
                S = 0.1 + np.random.sample(5) * 1.9
                for s in S:
                    coh = F.coherent(p, s, (m2 + 1j * m1))
                    coh.fourier('q')
                    for d in (0.5, 0.5 / np.sqrt(2)):
                        m_s = coh.mean_sigma(d)
                        assert np.abs(m_s[0] - m2) < coh._delta_x
                        assert np.abs(m_s[1] - 1 / s) < coh._delta_x
        print('Mean test ok')

    def test_bs(self):
        x = np.linspace(-10, 10, 2**12 + 1)
        vac = F.coherent(x, 1, 0)
        self.assert_almostequal(x[np.argmax(vac.y)], 0, n=2)
        alpha_list = (np.random.random(5) - 0.5) * 4
        for alpha in alpha_list:
            coh = F.coherent(x, 1, 1j * alpha)
            assert np.abs(x[np.argmax(coh.y)] - alpha) < coh._delta_x

            state1 = F._beam_splitter(coh, vac)
            state0 = state1.copy()
            state0.ptrace(axis=0, norm=True)
            state0.P()
            assert np.abs(x[np.argmax(state0.y)] - alpha / np.sqrt(2)) < state0._delta_x
            state1.ptrace(axis=1, norm=True)
            state1.P()
            assert np.abs(x[np.argmax(state1.y)] - alpha / np.sqrt(2)) < state1._delta_x
        print('BS test ok')

    def test_fft(self):
        x = np.linspace(-100, 100, 2**17 + 1)
        for _ in range(10):
            alpha = (np.random.random(2) - 0.5) * 5 * np.random.random()
            alpha = (alpha[0] + alpha[1] * 1j)
            state = F.coherent(x, 1, alpha)
            state.P()
            assert np.abs(state.x[np.argmax(state.y)] - np.imag(alpha)) < state._delta_x
        for _ in range(10):
            alpha = (np.random.random(2) - 0.5) * 5 * np.random.random()
            alpha = (alpha[0] + alpha[1] * 1j)
            state = F.coherent(x, 1, alpha)
            state.fourier('q')
            state.P()
            assert np.abs(state.x[np.argmax(state.y)] - np.real(alpha)) < state._delta_x
        print('FFT test ok')

    def test_bs_cat_cat(self):
        p = np.linspace(-50, 50, 2**11 + 1)
        c = F.cat(p, 1, 5)
        #state, _, _ = bs_meas(c, c)
        tensor = F._beam_splitter(c, c)
        state, _, _ = F._measure(tensor, quadrature='p')
        c.plot(quadrature='p', label='cat p')
        c.plot(quadrature='q', label='cat q')
        state.plot(quadrature='p', label='meas_p p')
        state.plot(quadrature='q', label='meas_p q')
        state, _, _ = F._measure(tensor, quadrature='q')
        state.plot(quadrature='p', label='meas_q p')
        state.plot(quadrature='q', label='meas_q q')
        plt.xlim((-10, 10))
        plt.legend()
        plt.savefig('test.png')

    def test_bs_vac_cat(self):
        p = np.linspace(-40, 40, 2**11 + 1)
        cat = F.cat(p, 1, 5)
        vac = F.coherent(p, 1, 0)
        tensor = F._beam_splitter(cat, vac)
        state, _, _ = F._measure(tensor, quadrature='p')
        cat.plot(quadrature='p', label='cat p')
        cat.plot(quadrature='q', label='cat q')
        state.plot(quadrature='p', label='meas_p p')
        state.plot(quadrature='q', label='meas_p q')
        state, _, _ = F._measure(tensor, quadrature='q')
        state.plot(quadrature='p', label='meas_q p')
        state.plot(quadrature='q', label='meas_q q')
        plt.xlim((-10, 10))
        plt.legend()

    def test_protocol_0(self):
        #p = np.linspace(-95, 95, 2**14 + 1)
        x = np.linspace(-20, 20, 2**11 + 1)
        delta = 0.5
        gkp = 2 * np.sqrt(np.pi)
        distance = gkp

        rounds = 5

        s = []
        for i in range(rounds):
            state, p = F.breed(x, delta, i, distance, result=0)
            s.append(state.mean_sigma(gkp)[1])
        print(np.array(s))

        s = []
        for i in range(rounds):
            phases = [0] * 2**i
            g = F.grid(x, 0.5, gkp, phases)
            s.append(g.mean_sigma(gkp)[1])
            if i == 4:
                g.plot()
        print(np.array(s))

        s = []
        for i in range(rounds):
            state_list = [F.grid(x, 0.5, gkp * np.sqrt(2)**i, [0])] * 2**i
            for j in range(i):
                state_list = F.grouper(state_list, 2)
                state_list, _, _ = list(zip(*[F.bs_meas(group, result=0) for group in state_list]))
            assert len(state_list) == 1
            state = state_list[-1]
            print(state._delta_x)
            s.append(state.mean_sigma(gkp)[1])
            if i == 4:
                state.plot()
                print(state._delta_x)
        print(np.array(s))
        plt.xlim((-10, 10))
        plt.show()
